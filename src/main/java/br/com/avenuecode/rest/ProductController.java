package br.com.avenuecode.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.avenuecode.rest.model.Product;
import br.com.avenuecode.rest.repository.ProductRepository;

@Controller
public class ProductController {
	
	@Autowired
	private ProductRepository producRepository;
	
	@RequestMapping("/")
	public String index() {
		return "index";
	}
	
	
	@RequestMapping("/getProducts")
	public String getProducts( Model model) {
		Iterable<Product> products = producRepository.findAll();
		
		model.addAttribute("product", products);
		
		return "product";
	}
	
	@RequestMapping("/createProducts")
	public String createProducts( Model model) {
		Iterable<Product> products = producRepository.findAll();
		
		model.addAttribute("product", products);
		
		return "product";
	}
	
	@RequestMapping("/deleteProducts")
	public String deleteProducts( Long id) {
		producRepository.delete(id);
		
		return "product";
	}
	
	@RequestMapping("/updateProducts")
	public String updateProducts( Long id) {
		producRepository.delete(id);

		return "product";
	}
	
}
