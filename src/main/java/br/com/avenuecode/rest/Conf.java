package br.com.avenuecode.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Conf {
	
	public static void main(String[] args) {
		SpringApplication.run(Conf.class, args);
	}

}
