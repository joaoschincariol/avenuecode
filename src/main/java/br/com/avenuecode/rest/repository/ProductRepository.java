package br.com.avenuecode.rest.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.avenuecode.rest.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long>{

}
