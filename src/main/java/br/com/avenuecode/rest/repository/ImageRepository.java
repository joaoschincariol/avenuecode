package br.com.avenuecode.rest.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.avenuecode.rest.model.Image;

public interface ImageRepository  extends CrudRepository<Image, Long>{

}
