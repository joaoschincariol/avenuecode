package br.com.avenuecode.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.avenuecode.rest.model.Image;
import br.com.avenuecode.rest.repository.ImageRepository;

@Controller
public class ImageController {
	
	@Autowired
	private ImageRepository imageRepository;
	
	@RequestMapping("/getImages")
	public String getImages( Model model) {
		Iterable<Image> images = imageRepository.findAll();
		
		model.addAttribute("image", images);
		
		return "image";
	}
	
	@RequestMapping("/createImages")
	public String createImages( Model model) {
		Iterable<Image> images = imageRepository.findAll();
		
		model.addAttribute("image", images);
		
		return "image";
	}
	
	@RequestMapping("/deleteImages")
	public String deleteImages( Long id) {
		imageRepository.delete(id);
		
		return "image";
	}
	
	@RequestMapping("/updateImages")
	public String updateImages( Long id) {

		return "image";
	}
}
